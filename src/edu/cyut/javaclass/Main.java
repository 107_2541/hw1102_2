package edu.cyut.javaclass;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int num = sc.nextInt();
        String st = num + "";


        StringBuilder s = new StringBuilder(st);
        s.reverse();
        int x = 0;

        for (int i = 0; i < st.length(); i++) {
            if (st.charAt(i) != s.charAt(i)) {
                System.out.println(st + "不是迴文數");
                break;
            } else {
                x++;
            }
        }
        if (x == st.length()) {
            System.out.println(st + "是迴文數");
        }
    }
 
}